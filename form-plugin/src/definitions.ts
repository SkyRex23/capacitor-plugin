declare module '@capacitor/core' {
  interface PluginRegistry {
    FormPlugin: FormPluginPlugin;
  }
}

export interface FormPluginPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  showForm(): Promise<any>;
}
