import Foundation
import Capacitor
import SwiftUI


@objc(FormPlugin)
public class FormPlugin: CAPPlugin {

    @objc func echo(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.success([
            "value": value
        ])
    }
}
